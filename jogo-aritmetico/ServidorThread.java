import java.io.*;
import java.net.*;
import java.util.ArrayList;

public class ServidorThread extends Thread {
   Socket conexao;
   String nome;
   int pontuacao = 0;
   ServidorAritmeticaMU servidorAMU;
	BufferedReader doCliente;   

   DataOutputStream paraCliente;

   public ServidorThread( Socket conexao, ServidorAritmeticaMU servidorAMU ) {
      try {
         this.conexao = conexao;
         this.servidorAMU = servidorAMU;
	      doCliente = new BufferedReader(
			                  new InputStreamReader(
                              conexao.getInputStream()
                           )
                     );
	      paraCliente = new DataOutputStream(
                              conexao.getOutputStream()
                       );
      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   public void notificar(String mensagem) {
      try {
    	  
         paraCliente.writeBytes(mensagem);
      } catch (IOException ioe) {
         ioe.printStackTrace();
      }      
   }

   public void pontuacao() {
      try {
         paraCliente.writeBytes("pontuacao: "+pontuacao+"\n");
      } catch (IOException ioe) {
         ioe.printStackTrace();
      } 
   }

   /**
    * Método responsável por disparar uma mensagem para todos os clientes Sockets
    * @author Kelvin Santiago 
    * @throws IOException
    */
  
   public void mostrarPontosDosClientesParaTodosClientes() throws IOException{
       
	   ArrayList<ServidorThread> servidores = servidorAMU.getServidores();
       
        for (ServidorThread servidorThread: servidores) {
        	servidorThread.paraCliente.writeBytes(
        			"\n"+
        			"================= RANKING ================="+
        			"\n"+
 					"NOME"+
 					"\t\t"+
 					"PONTOS"+
 					"\n"
        			);
        	for (ServidorThread cliente: servidores) {
        		servidorThread.
        	paraCliente.writeBytes(
        			               cliente.nome +
        			               "\t\t"+
        			               cliente.pontuacao+
        			               "\n"
        			               );
        	}
        	servidorThread.
        	paraCliente.writeBytes("==========================================="+
        			               "\n");
        }
        
   }
   
   @Override
   public void run() {
      try {
    	  
         paraCliente.writeBytes("Forneca seu nome: ");
         nome = doCliente.readLine();

         while ( servidorAMU.getStatus() == 0 ) {
            paraCliente.writeBytes("O jogo ainda nao comecou. "+
                                   "Digite '/iniciar' para comecar ou "+
                                   "'/listar' para listar os jogadores\n");
            String comando = doCliente.readLine();
            if ( comando.equals("/listar") ) {
               ArrayList<ServidorThread> servidores 
                  = servidorAMU.getServidores();
               for (ServidorThread servidorThread: servidores) {
                  paraCliente.writeBytes(servidorThread.nome+"\n");
               }
            }
            else
               servidorAMU.iniciar();
         }
         String resposta;
         Desafio desafio;
         while ( servidorAMU.getStatus() == 1 ) { 
            desafio = servidorAMU.lancarDesafio();
			   resposta = doCliente.readLine();
            if ( resposta.equals( desafio.resposta) ) {
               pontuacao++;
               paraCliente.writeBytes("VOCE ACERTOU! :D\n\n");
               mostrarPontosDosClientesParaTodosClientes();
            }
            else {
               paraCliente.writeBytes("VOCE ERROU! :(\n");         
               pontuacao--;
               pontuacao();
            }
         }
		   conexao.close();
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
}